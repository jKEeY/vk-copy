import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {
  v4 as uuidv4
} from 'uuid'
import {
  config
} from '../../config';
import {
  refreshTokenHelper
} from '../services'

async function generatePasswordHash(pass) {
  const salt = await bcrypt.genSaltSync(10)
  const hash = await bcrypt.hashSync(pass, salt);

  return hash
}

function issueToken(data, options = {}) {
  return jwt.sign(data, config.secret, options)
}

async function issueTokenPair(userId) {
  const refreshToken = uuidv4()

  await refreshTokenHelper.add({
    user_id: userId,
    token: refreshToken
  })

  const body = {
    token: jwt.sign({
      id: userId
    }, config.secret),
    refreshToken,
  }

  return body
}

const arrayValidate = (date = [], fn) => Array.isArray(data) && data.every(fn)

export {
  generatePasswordHash,
  issueToken,
  issueTokenPair,
  arrayValidate
}