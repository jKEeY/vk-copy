
exports.up = function(knex) {
  return knex.schema.createTable('group_post', tbl => {
    tbl.increments('id').primary();
    tbl.integer('group_id')
      .notNullable()
      .unique()
    tbl.integer('post_id')
      .notNullable()
      .unique()
    tbl.timestamp(true, true)
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('group_post');
};
