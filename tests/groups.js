import request from "supertest";
import test from "ava";

import {
  server
} from "../src/index";
import {
  issueToken,
  arrayValidate
} from "../src/helpers";

const app = request.agent(server);

const authLine = `Bearer ${issueToken({ id: 1 })}`;

test.afterEach(() => {
  server.close();
});

test("Create group", async t => {
  const res = await app
    .post('/group')
    .send({
      name: 'Test',
      info: 'lorem',
    })
    .set('Authorization', authLine)
    .expect(200)

  t.is(res.status, 200)
  t.truthy(typeof res.body.group === 'string')
});

test("Get groups by user id", async t => {
  const res = await app
    .get('/groups')
    .set('Authorization', authLine)
    .expect(200)

  t.is(res.status, 200)
  t.truthy(arrayValidate(res.body.groups, (group) => {
    group.author_id === 1
  }))
});