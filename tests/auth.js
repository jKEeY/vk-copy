import request from 'supertest';
import test from 'ava';
import {
  server
} from '../src/index';
import {
  issueToken
} from '../src/helpers';

const app = request.agent(server)

test.afterEach(() => {
  server.close()
})

test('User can succesfully login', async (t) => {
  const res = await app
    .post('/auth/login')
    .send({
      login: "jKEeY",
      password: "123"
    })
    .expect(200)

  t.truthy(typeof res.body.token === 'string');
  t.truthy(typeof res.body.refreshToken === 'string');

  const refreshToken = await app
    .post('/auth/refresh')
    .send({
      refreshToken: res.body.refreshToken
    })
    .expect(200)

  t.truthy(typeof refreshToken.body.token === 'string');
  t.truthy(typeof refreshToken.body.refreshToken === 'string');
})

test('User gets 403 on invalid credentials', async t => {
  const res = await app
    .post('/auth/login')
    .send({
      login: "INVALID",
      password: "INVALID"
    })
    .expect(403)
  t.is(res.status, 403)
})

test('User receives 401 on expired token', async t => {
  const expiredToken = issueToken({
    id: 1
  }, {
    expiresIn: '1ms'
  })

  const res = await app
    .get('/profile')
    .set('Authorization', `Bearer ${expiredToken}`)
    .expect(401)

  t.is(res.status, 401)
})

test('User cat get new access token using refresh token', async t => {
  const res = await app
    .post('/auth/refresh')
    .send({
      refreshToken: 'TOKEN'
    })
    .expect(200)

  t.is(res.status, 200)
})

test('User get 404 on invalid refresh token', async t => {
  const res = await app
    .post('/auth/refresh')
    .send({
      refreshToken: 'TEST'
    })
    .expect(404)

  t.is(res.status, 404)
})

test('User can use refresh token only once', async t => {
  const firstRefreshToken = await app
    .post('/auth/refresh')
    .send({
      refreshToken: 'TOKEN_ONCE'
    })
    .expect(200)

  t.truthy(typeof firstRefreshToken.body.token === 'string');
  t.truthy(typeof firstRefreshToken.body.refreshToken === 'string');

  const lastRefreshToken = await app
    .post('/auth/refresh')
    .send({
      refreshToken: 'TOKEN_ONCE'
    })
    .expect(404)

  t.is(lastRefreshToken.status, 404);
})

test('Refresg token becore inalid on logout', async t => {
  const expiredToken = issueToken({
    id: 2
  })
  const lres = await app
    .post('/auth/logout')
    .set('Authorization', `Bearer ${expiredToken}`)
    .expect(200)

  t.is(lres.status, 200);

  const res = await app
    .post('/auth/refresh')
    .send({
      refreshToken: 'LOGOUT'
    })
    .expect(404)

  t.is(res.status, 404);
})

test('Multiple refresh tokens are valid', async t => {
  const firstRes = await app
    .post('/auth/login')
    .send({
      login: "jKEeY",
      password: "123"
    })
    .expect(200)

  t.truthy(typeof firstRes.body.token === 'string');
  t.truthy(typeof firstRes.body.refreshToken === 'string');

  const lastRes = await app
    .post('/auth/login')
    .send({
      login: "test",
      password: "321"
    })
    .expect(200)

  t.truthy(typeof lastRes.body.token === 'string');
  t.truthy(typeof lastRes.body.refreshToken === 'string');

  const firstRefreshToken = await app
    .post('/auth/refresh')
    .send({
      refreshToken: firstRes.body.refreshToken
    })
    .expect(200)

  t.truthy(typeof firstRefreshToken.body.token === 'string');
  t.truthy(typeof firstRefreshToken.body.refreshToken === 'string');

  const lastRefreshToken = await app
    .post('/auth/refresh')
    .send({
      refreshToken: lastRes.body.refreshToken
    })
    .expect(200)

  t.truthy(typeof lastRefreshToken.body.token === 'string');
  t.truthy(typeof lastRefreshToken.body.refreshToken === 'string');
})