
exports.up = function(knex) {
  
  return knex.schema.createTable('posts', tbl => {
    tbl.increments('id').primary();
    tbl.string('title')
      .notNullable()
    tbl.text('text', 300)
    tbl.timestamp(true, true)
  })

};

exports.down = function(knex) {
  knex.schema.dropTable('posts');
};
