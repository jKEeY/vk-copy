import knex from 'knex';
import config from '../../knexfile.js'

const db = knex(config.development);

class UserHelper {
  /**
   * @param user 'User Table'
   * @return id increments
  */
  async add(user) {
    const [id] = await db('users').insert(user)

    return id
  }
  async findOnLogin(login)  {
    const user = await db('users')
      .where('login', login)
      .select('login', 'id', 'password')
    if (user.length) return user[0];
    return null
  }
  async findById(id) {
    const user = await db('users')
      .where('id', id)
      .select('login', 'id', 'first_name', 'last_name', 'date_of_birth', 'image_url')

    if (user.length) return user[0];
    return null
  }
}   

export { UserHelper }