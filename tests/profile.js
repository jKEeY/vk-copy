import request from "supertest";
import test from "ava";
import { server } from "../src/index";
import { issueToken } from "../src/helpers";

const app = request.agent(server);

const authLine = `Bearer ${issueToken({ id: 1 })}`;

test.afterEach(() => {
  server.close();
});

test("Get user profile", async (t) => {
  const res = await app
    .get("/profile")
    .set("Authorization", authLine)
    .expect(200);

  const keysProfileArray = Object.keys(res.body.profile).length;

  t.truthy(typeof res.body.profile === "object");
  t.is(keysProfileArray, 6);
});
