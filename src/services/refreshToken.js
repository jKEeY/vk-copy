import knex from 'knex';
import config from '../../knexfile.js'

const db = knex(config.development);

class RefreshTokenHelper {
  async find(query='') {
    const token = await db('refresh_token').where(query);
    if (token.length) return token[0]

    return null
  }
  async add(entry) {
    return await db('refresh_token').insert(entry)
  }

  async remove(query) {
    return await db('refresh_token').where(query).del();
  }
}

export { RefreshTokenHelper }