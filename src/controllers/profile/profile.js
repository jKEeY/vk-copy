import { userHelper } from '../../services'

const profile = async (req, res) => {
  const {id} = req.user;
  const user = await userHelper.findById(id);

  if (!user) return res.status(404).json({message: 'Not Found'})

  return res.status(200).json({ profile: user })
}

export { profile }