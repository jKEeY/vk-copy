
exports.up = function(knex) {
  return knex.schema.createTable('groups', tbl => {
    tbl.increments('id').primary();
    tbl.string('name')
      .notNullable()
      .unique()
    tbl.text('info', 128)
    tbl.integer('author_id')
      .notNullable()
      .unique()
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('groups');
};
