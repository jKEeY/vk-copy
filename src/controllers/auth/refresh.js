import { refreshTokenHelper } from '../../services'
import { issueTokenPair } from '../../helpers'

const refresh = async (req, res) => {
  const { refreshToken } = req.body;

  const dbToken = await refreshTokenHelper.find({ token: refreshToken })
  if (!dbToken) return res.status(404).json({});
  await refreshTokenHelper.remove({ token: refreshToken })
  const body = await issueTokenPair(dbToken.user_id)

  return res.status(200).json(body)
}

export { refresh }