import request from "supertest";
import test from "ava";

import { server } from "../src/index";
import { issueToken } from "../src/helpers";

const app = request.agent(server);

const authLine = `Bearer ${issueToken({ id: 1 })}`;

test.afterEach(() => {
  server.close();
});

test.todo("Create post by user");
test.todo("Create post by group");
test.todo("Get posts by user id");
test.todo("Get posts by group id");
test.todo("Update post by post id and group id");
test.todo("Update post by post id and user id");
test.todo(
  "Get 403 status if the update post user is not based on the author`s id"
);
test.todo(
  "Get 403 status if the update post group is not based on the author`s id group"
);

test.todo("Delete post by post id and group id");
test.todo("Delete post by post id and user id");
test.todo(
  "Get 403 status if the delete post user is not based on the author`s id"
);
test.todo(
  "Get 403 status if the delete post group is not based on the author`s id group"
);
