// Update with your config settings.

module.exports = {
  development: {
    client: 'sqlite3',
    connection: {
      filename: './database/vk-copy.db3'
    }
  },
  useNullAsDefault: true
};
