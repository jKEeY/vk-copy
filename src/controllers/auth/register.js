import { generatePasswordHash } from '../../helpers'
import { userHelper } from '../../services'

const register = async (req, res) => {
  const {
    login,
    password,
    first_name,
    last_name,
    date_of_birth,
  } = req.body;

  const userTmp = {
    login,
    password: generatePasswordHash(password),
    first_name,
    last_name,
    date_of_birth,
    image_url: null
  }

  const user = await userHelper.add(userTmp);

  const refreshToken = uuidv4()
  const body = {
    token: jwt.sign({ id: user.id }, config.secret),
    refreshToken,
    user,
  }

  return res.status(200).json(body);
}

export { register }