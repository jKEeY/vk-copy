
exports.up = function(knex) {
  return knex.schema.createTable('refresh_token', tbl => {
    tbl.increments('id').primary();
    tbl.integer('user_id')
      .notNullable()
    tbl.text('token', 500)
      .notNullable()
    tbl.timestamp(true, true)
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('refresh_token')
};
