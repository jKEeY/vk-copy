const { generatePasswordHash } = require('../src/helpers/index');

exports.seed = (knex) => {
  return knex('users').del()
    .then(async () => {
      return knex('users').insert([
        {
          login: 'jKEeY',
          password: await generatePasswordHash('123'),
          first_name: 'Ilya',
          last_name: 'Vegner',
          date_of_birth: '2020-02-01',
          image_url: 'google.com'
        },
        {
          login: 'test',
          password: await generatePasswordHash('321'),
          first_name: 'qwe',
          last_name: 'ewq',
          date_of_birth: '2021-02-01',
          image_url: 'google.com/'
        }
      ]);
    });
};
