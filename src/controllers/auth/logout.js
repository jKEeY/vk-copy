import { refreshTokenHelper } from '../../services'

const logout = async (req, res) => {
  const { id } = req.user;
  await refreshTokenHelper.remove({ user_id: id });

  return res.status(200).json({ success: true });
}

export { logout }