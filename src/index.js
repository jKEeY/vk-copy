import express from 'express'
import jwt from 'jsonwebtoken'
import {
  login,
  register,
  refresh,
  logout
} from './controllers/auth'
import {
  profile
} from './controllers/profile'
import {
  config
} from '../config'

const app = express();

app.use(express.json());

const PORT = 3001;

function protectedRouter(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).json({
      code: 1,
      error: 'Forbiden'
    })
  }
  jwt.verify(token, config.secret, (err, user) => {
    if (err) return res.status(401).json({
      code: 401,
      error: 'Token is not valid'
    })

    req.user = user;
    next();
  })
}

app.get('/profile', protectedRouter, profile)

app.post('/auth/login', login)
app.post('/auth/register', register)
app.post('/auth/refresh', refresh)
app.post('/auth/logout', protectedRouter, logout)

const server = app.listen(PORT, () => {
  console.log('Server listening on port ' + PORT)
})

export {
  server
}