
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('refresh_token').del()
    .then(function () {
      // Inserts seed entries
      return knex('refresh_token').insert([
        { user_id: 1, token: 'TOKEN'},
        { user_id: 1, token: 'TOKEN_ONCE'},
      ]);
    });
};
