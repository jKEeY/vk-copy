import { compareSync } from 'bcryptjs'
import { userHelper } from '../../services'
import { issueTokenPair } from '../../helpers'

const login = async (req, res) => {
  const { login, password } = req.body;

  const user = await userHelper.findOnLogin(login);
  if (!user || !compareSync(password, user.password)) {
    return res.status(403).send({ error: 'User not Found'})
  }
  
  const body = await issueTokenPair(user.id)

  return res.status(200).json(body)
}

export { login }