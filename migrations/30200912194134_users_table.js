
exports.up = function(knex) {
  return knex.schema.createTable('users', tbl => {
    tbl.increments('id').primary();
    tbl.string('login', 128)
      .notNullable()
      .unique()
    tbl.string('password', 128)
      .notNullable()
    tbl.string('first_name', 128)
      .notNullable()
    tbl.string('last_name', 128)
      .notNullable()
    tbl.date('date_of_birth')
    tbl.text('image_url', 228)
    tbl.timestamp(true, true)
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('users');
};
