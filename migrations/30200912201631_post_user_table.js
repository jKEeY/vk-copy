
exports.up = function(knex) {
  return knex.schema.createTable('post_user', tbl => {
    tbl.increments('id').primary();
    tbl.integer('user_id')
      .notNullable()
      .unique()
    tbl.integer('post_id')
      .notNullable()
      .unique()
    tbl.timestamp(true, true)
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('post_user');
};
